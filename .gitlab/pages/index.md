<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
<style>
  body {
    font-family: 'Roboto', sans-serif;
    background: #0E0B16;
    color: #E7DFDD;
  }
  a, a:visited {
    color: #007B9C;
  }
  a:hover {
    color: #006580;
  }
</style>

# Selectry
<!-- * [Code Coverage Report](./coverage) -->
* [Latest documentation](https://selectry.herokuapp.com/)
* [Docs from master](https://selectry-stg.herokuapp.com/)
