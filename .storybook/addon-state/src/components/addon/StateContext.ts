
import * as React from 'react'

import { DeepReadonly } from '../../helpers/DeepReadonly'

export type StateModifierFunction<T> = (oldVal: DeepReadonly<T>) => T | DeepReadonly<T> | (Partial<T> & DeepReadonly<T>)
export type StateModifier<T> = Partial<T> | StateModifierFunction<T>

export interface StoryState<T> {
  state: DeepReadonly<T>
  setState: (newVal: StateModifier<T>) => void
}

const StateContext = React.createContext<StoryState<any>>({ state: null, setState: () => {} })

export const StateProvider = StateContext.Provider

export interface StateConsumerType {
  <T>(props: { children: (val: StoryState<T>) => JSX.Element }): JSX.Element
}

export const StateConsumer = StateContext.Consumer as StateConsumerType
