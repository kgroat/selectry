
import * as React from 'react'

import { StoryContext } from '@storybook/addons'
import { STORY_CHANGED } from '@storybook/core-events'
import { Channel } from '@storybook/channels'

import { STATE_CHANGE_EVENT, RESET_STATE_EVENT } from '../../helpers/constants'
import { deepFreeze } from '../../helpers/deepFreeze'
import { DeepReadonly } from '../../helpers/DeepReadonly'
import { StateModifier, StoryState, StateProvider } from './StateContext'

interface StateWrapperProps<T> {
  initialState: T
  channel: Channel
  context: StoryContext
  getStory: (...args: any[]) => any
}

interface StateWrapperState<T> {
  value: DeepReadonly<T>
}

export class StateWrapper<T> extends React.Component<StateWrapperProps<T>, StateWrapperState<T>> {
  state = {
    value: deepFreeze(this.props.initialState),
  }

  componentDidMount () {
    const { channel, initialState } = this.props
    channel.emit(STATE_CHANGE_EVENT, initialState)

    channel.addListener(RESET_STATE_EVENT, this._reset)
    channel.addListener(STATE_CHANGE_EVENT, this._handleStateChange)
    channel.addListener(STORY_CHANGED, this._reset)
  }

  componentWillUnmount () {
    const { channel } = this.props

    channel.removeListener(RESET_STATE_EVENT, this._reset)
    channel.removeListener(STATE_CHANGE_EVENT, this._handleStateChange)
    channel.removeListener(STORY_CHANGED, this._reset)
  }

  private _reset = () => this._setState(this.props.initialState)

  private _handleStateChange = (newValue: T | DeepReadonly<T>) => {
    const { value } = this.state
    if (value === newValue) {
      return
    }

    this.setState({ value: deepFreeze(newValue as T) })
  }

  private _setState = (modifier: StateModifier<T>) => {
    const { channel } = this.props
    const { value } = this.state
    const newValue = typeof modifier === 'function'
      ? modifier(value)
      : { ...value, ...modifier }

    this.setState({ value: deepFreeze(newValue as T) })
    channel.emit(STATE_CHANGE_EVENT, newValue)
  }

  private _buildContext = (): StoryState<T> => ({
    state: this.state.value,
    setState: this._setState,
  })

  render () {
    return (
      <StateProvider value={this._buildContext()}>
        {this.props.getStory()}
      </StateProvider>
    )
  }
}