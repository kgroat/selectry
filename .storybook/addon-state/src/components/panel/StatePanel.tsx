
import * as React from 'react'

import { Channel } from '@storybook/channels'
import { STORY_CHANGED } from '@storybook/core-events'
import { Placeholder, ActionBar, Link, ScrollArea } from '@storybook/components'

import { STATE_CHANGE_EVENT, RESET_STATE_EVENT } from '../../helpers/constants'

interface Props {
  channel: Channel
  active: boolean
}

interface State<T> {
  value: T | undefined
}

export class StatePanel<T> extends React.Component<Props, State<T>> {
  state: State<T> = {
    value: undefined,
  }

  componentDidMount () {
    this.props.channel.addListener(STATE_CHANGE_EVENT, this._setValue)
    this.props.channel.addListener(STORY_CHANGED, this._killValue)
  }

  componentWillUnmount () {
    this.props.channel.removeListener(STATE_CHANGE_EVENT, this._setValue)
    this.props.channel.removeListener(STORY_CHANGED, this._killValue)
  }

  private _setValue = (value: T) => this.setState({ value })
  private _killValue = () => this.setState({ value: undefined })
  private _resetValue = () => this.props.channel.emit(RESET_STATE_EVENT)

  private _renderValue = () => {
    const { value } = this.state

    if (value === undefined) {
      return (
        <Placeholder>
          <React.Fragment>No state found</React.Fragment>
          <React.Fragment>
            Learn how to&nbsp;
            <Link href='https://gitlab.com/kgroat/selectry' target='_blank' withArrow>create stories with state</Link>
          </React.Fragment>
        </Placeholder>
      )
    }

    return (
      <pre>
        <code>
          {JSON.stringify(value, null, 2)}
        </code>
      </pre>
    )
  }

  render () {
    if (!this.props.active) {
      return false
    }

    return (
      <React.Fragment>
        <ScrollArea horizontal vertical>
          {this._renderValue()}
        </ScrollArea>
        <ActionBar actionItems={[
          { title: 'reset', onClick: this._resetValue }
        ]} />
      </React.Fragment>
    )
  }
}
