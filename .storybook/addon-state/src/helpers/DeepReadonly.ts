
export type DeepReadonly<T> = 
  T extends Function ? T :
  T extends Map<infer K, infer V> ? ReadonlyMap<DeepReadonly0<K>, DeepReadonly0<V>> :
  T extends Set<infer K> ? ReadonlySet<DeepReadonly0<K>> :
  T extends (infer V)[] ? ReadonlyArray<DeepReadonly0<V>> :
  T extends object ? { readonly [P in keyof T]: DeepReadonly<T[P]> } :
  T

type DeepReadonly0<T> = 
  T extends Function ? T :
  T extends Map<infer K, infer V> ? ReadonlyMap<DeepReadonly1<K>, DeepReadonly1<V>> :
  T extends Set<infer K> ? ReadonlySet<DeepReadonly1<K>> :
  T extends (infer V)[] ? ReadonlyArray<DeepReadonly1<V>> :
  T extends object ? { readonly [P in keyof T]: DeepReadonly<T[P]> } :
  T

type DeepReadonly1<T> = 
  T extends Function ? T :
  T extends Map<infer K, infer V> ? ReadonlyMap<DeepReadonly2<K>, DeepReadonly2<V>> :
  T extends Set<infer K> ? ReadonlySet<DeepReadonly2<K>> :
  T extends (infer V)[] ? ReadonlyArray<DeepReadonly2<V>> :
  T extends object ? { readonly [P in keyof T]: DeepReadonly<T[P]> } :
  T

type DeepReadonly2<T> = 
  T extends Function ? T :
  T extends Map<infer K, infer V> ? ReadonlyMap<DeepReadonly3<K>, DeepReadonly3<V>> :
  T extends Set<infer K> ? ReadonlySet<DeepReadonly3<K>> :
  T extends (infer V)[] ? ReadonlyArray<DeepReadonly3<V>> :
  T extends object ? { readonly [P in keyof T]: DeepReadonly<T[P]> } :
  T

type DeepReadonly3<T> = 
  T extends Function ? T :
  T extends Map<infer K, infer V> ? ReadonlyMap<DeepReadonly4<K>, DeepReadonly4<V>> :
  T extends Set<infer K> ? ReadonlySet<DeepReadonly4<K>> :
  T extends (infer V)[] ? ReadonlyArray<DeepReadonly4<V>> :
  T extends object ? { readonly [P in keyof T]: DeepReadonly<T[P]> } :
  T

type DeepReadonly4<T> = 
  T extends Function ? T :
  T extends Map<infer K, infer V> ? ReadonlyMap<DeepReadonly5<K>, DeepReadonly5<V>> :
  T extends Set<infer K> ? ReadonlySet<DeepReadonly5<K>> :
  T extends (infer V)[] ? ReadonlyArray<DeepReadonly5<V>> :
  T extends object ? { readonly [P in keyof T]: DeepReadonly<T[P]> } :
  T

type DeepReadonly5<T> = 
  T extends Function ? T :
  T extends Map<infer K, infer V> ? ReadonlyMap<Readonly<K>, Readonly<V>> :
  T extends Set<infer K> ? ReadonlySet<Readonly<K>> :
  T extends (infer V)[] ? ReadonlyArray<Readonly<V>> :
  T extends object ? { readonly [P in keyof T]: DeepReadonly<T[P]> } :
  T