
export const ADDON_ID = 'addon-state'
export const PANEL_ID = `${ADDON_ID}/panel`

export const STATE_CHANGE_EVENT = `${ADDON_ID}/state-change`
export const RESET_STATE_EVENT = `${ADDON_ID}/reset-state`
