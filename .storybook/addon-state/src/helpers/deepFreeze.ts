
import { DeepReadonly } from './DeepReadonly'

// export function deepFreeze<T extends DeepReadonly<{} | [] | ((...args: any[]) => any)>> (o: T): T
export function deepFreeze<T extends {} | [] | ((...args: any[]) => any)> (o: T): DeepReadonly<T> {
  const o2: { [key: string]: any } = o

  Object.getOwnPropertyNames(o).forEach(prop => {
    const val = o2[prop]
    if (o2.hasOwnProperty(prop)
    && val !== null
    && (typeof val === "object" || typeof val === "function")
    && !Object.isFrozen(val)) {
      o2[prop] = deepFreeze(val)
    }
  })

  Object.freeze(o)
  
  return o as DeepReadonly<T>
}