
import * as React from 'react'

import addons, { StoryGetter, StoryContext, WrapperSettings, } from '@storybook/addons'
import { StateWrapper } from './components/addon/StateWrapper'
export { StateConsumer } from './components/addon/StateContext'

export function withState<T> (origState: T) {
  return (getStory: StoryGetter, context: StoryContext, settings: WrapperSettings = {} as any) => {
    const channel = addons.getChannel()
    const { parameters = {}, options = {} } = settings
    const initialState = (parameters.initialState || options.initialState || origState) as T

    return <StateWrapper channel={channel} initialState={initialState} getStory={getStory} context={context} />
  }
}
