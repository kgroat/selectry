import * as React from 'react'
import addons from '@storybook/addons'
import { ADDON_ID, PANEL_ID } from './helpers/constants'
import { StatePanel } from './components/panel/StatePanel'

addons.register(ADDON_ID, () => {
  addons.addPanel(PANEL_ID, {
    title: 'State',
    render: ({ active, key }) => <StatePanel key={key} active={active} channel={addons.getChannel()} />,
  });
})