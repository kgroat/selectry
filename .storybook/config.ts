
import { configure, addParameters } from '@storybook/react'
import { create } from '@storybook/theming'
import './overrides.pcss'

// Storybook options
addParameters({
  options: {
    theme: create({
      base: 'dark',
      brandTitle: 'Selectry',
    }),
    name: 'Selectry',
    showPanel: true,
    panelPosition: 'right',
  },
});

configure(require.context('../src', true, /\.stories\.([tj]sx?|mdx)$/), module)
