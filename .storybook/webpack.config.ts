
import { Configuration, RuleSetLoader } from 'webpack'

import { styleRule } from '../config/webpack/styles'

export default async ({ config }: { config: Configuration }) => {
  const tsRule = config.module!.rules!.find(r => r.test!.toString().includes('ts'))!
  const tsLoader = (tsRule.use! as RuleSetLoader[]).find(u => u.loader!.includes('ts-loader'))!
  tsLoader.options = {
    ...(tsLoader.options as any),
    configFile: require.resolve('../config/tsconfig.webpack.json'),
  }
  config.module!.rules!.push(styleRule(false, true));
  return config;
}
