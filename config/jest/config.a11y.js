const origConfig = require('./config')

module.exports = {
  ...origConfig,
  testMatch: [
    '<rootDir>/src/**/*.a11y.ts?(x)',
  ],
}