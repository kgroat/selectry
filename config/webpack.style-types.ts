
import * as webpack from 'webpack'
import * as path from 'path'

// This has to be done because there aren't typings for disable-output-webpack-plugin
const DisableOutputWebpackPlugin = require('disable-output-webpack-plugin')

import { styleRule } from './webpack/styles'

function createOnlyStyleTypesConfig (): webpack.Configuration {
  return {
    bail: false,
    mode: 'none',
    entry: [path.join(__dirname, '../src/styles.pcss')],
    output: {
      path: path.join(__dirname, '../dist'),
    },
    resolve: {
      extensions: [
        '.js',
        '.pcss',
      ],
    },
    module: {
      rules: [
        styleRule(true),
      ],
    },
    plugins: [
      new DisableOutputWebpackPlugin(),
    ],
  }
}

export default createOnlyStyleTypesConfig
