
import * as webpack from 'webpack'
import * as path from 'path'

import * as MiniCssExtractPlugin from 'mini-css-extract-plugin'

import { styleRule } from './webpack/styles'

function createOnlyStylesConfig (): webpack.Configuration {
  return {
    bail: true,
    mode: 'production',
    entry: path.join(__dirname, '../src/css.ts'),
    output: {
      library: 'selectry',
      libraryTarget: 'commonjs',
      libraryExport: 'default',
      path: path.join(__dirname, '../dist'),
      filename: 'css.js',
    },
    resolve: {
      extensions: [
        '.ts',
        '.tsx',
        '.pcss',
      ],
    },
    module: {
      rules: [
        styleRule(false),
        {
          test: /\.tsx?$/,
          use: [
            'ts-loader',
          ],
        }
      ],
    },
    plugins: [
      new MiniCssExtractPlugin({
        filename: 'styles.css',
      })
    ]
  }
}

export default createOnlyStylesConfig
