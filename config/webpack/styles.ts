import * as webpack from 'webpack'

import * as MiniCssExtractPlugin from 'mini-css-extract-plugin'

export const styleRule = (onlyTypes = false, storybook = false): webpack.RuleSetRule => ({
  test: /\.p?css$/,
  exclude: /node_modules/,
  use: [
    onlyTypes || storybook ? 'style-loader' : MiniCssExtractPlugin.loader,
    onlyTypes ? 'null-loader' : {
      loader: 'css-loader',
      options: {
        modules: {
          mode: 'local',
          localIdentName: 'selectry-[local]',
        },
        localsConvention: 'camelCaseOnly',
        importLoaders: 2,
      },
    },
    'typed-css-modules-loader?noEmit&camelCase',
    {
      loader: 'postcss-loader',
      options: {
        ident: 'postcss',
        plugins: () => [
          !onlyTypes ? '' : require('stylelint')({ }),
          !onlyTypes ? '' : require('postcss-reporter')({ clearReportedMessages: true }),
          require('postcss-import'),
          require('postcss-mixins'),
          require('postcss-each'),
          require('postcss-for'),
          require('postcss-simple-vars'),
          require('postcss-calc'),
          require('postcss-flexbugs-fixes'),
          // require('postcss-functions')({
          //   functions: pcssFunctions,
          // }),
          require('postcss-color-function'),
          require('postcss-preset-env'),
          require('postcss-nested'),
          require('autoprefixer')({
            flexbox: 'no-2009',
          }),
        ].filter(i => i),
      },
    },
  ].filter(l => l),
})
