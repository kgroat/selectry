const utils = require('nps-utils')
const jestConfig = require.resolve('./config/jest/config.js')
const a11yJestConfig = require.resolve('./config/jest/config.a11y.js')
const onlyStyleTypesWebpackConfig = require.resolve('./config/webpack.style-types.ts')
const onlyStylesWebpackConfig = require.resolve('./config/webpack.styles.ts')

const webpackTsconfig = `TS_NODE_PROJECT=${require.resolve('./config/tsconfig.webpack.json')}`
const webpack = `${webpackTsconfig} NODE_ENV="production" webpack`

const CI = process.env.CI

function addForCI (cmd, toAdd) {
  return CI ? `${cmd} ${toAdd}` : cmd
}

function ifServerStarted (ifStarted, ifNotStarted) {
  if (CI) {
    return ifNotStarted
  }

  return `
    if curl -s "http://localhost:\${PORT:-3000}" >/dev/null; then
      ${ifStarted}
    else
      ${ifNotStarted}
    fi
  `
}

module.exports = {
  scripts: {
    default: {
      script: utils.concurrent.nps('storybook', 'build.css.types.watch', 'test.watch'),
      description: 'Starts webpack dev server.',
    },
    watch: {
      script: utils.concurrent.nps('build.css.types.watch', 'test.watch'),
    },
    storybook: {
      script: `${webpackTsconfig} start-storybook -p \${PORT:-3000}`,
      description: 'Starts storybook so you can play with the component.',
      build: {
        script: `${webpackTsconfig} build-storybook -o public`,
        description: 'Compiles storybook into static files',
      },
    },
    build: {
      script: 'nps build.clean build.css.types build.code build.css',
      description: 'Builds a production bundle of the library.',
      clean: {
        script: `rm -rf 'dist'`,
        description: 'removes the output directory'
      },
      css: {
        script: `${webpack} --config ${onlyStylesWebpackConfig}`,
        description: 'Buids the css modules.',
        types: {
          script: `${webpack} --config ${onlyStyleTypesWebpackConfig}`,
          description: 'Generates typescript typings for css modules.',
          watch: {
            script: 'nps "build.css.types -w"',
            description: 'Generates typescript typings for css modules in watch mode.',
          },
        },
      },
      code: {
        script: `tsc`,
        description: 'typechecks & builds the library',
      },
    },
    a11y: {
      script: `jest --config ${a11yJestConfig}`,
      description: 'Starts the a11y tests for the library.',
      watch: {
        script: `nps "test --watchAll"`,
        description: 'Runs the tests in watch mode',
      },
    },
    test: {
      script: `jest --config ${jestConfig}`,
      description: 'Starts the jest tests for the library.',
      watch: {
        script: `nps "test --watchAll"`,
        description: 'Runs the tests in watch mode',
      },
      fix: {
        script: `nps "test -u"`,
        description: 'Runs the tests in snapshot-update mode.',
      }
    },
    lint: {
      script: 'nps lint.ts lint.css',
      description: 'Runs tslint and stylelint.',
      ts: {
        script: 'tslint --project .',
        description: 'Runs tslint for the entire project.',
        fix: {
          script: 'nps "lint.ts --fix"',
          description: 'Runs tslint in fix mode.',
        },
      },
      css: {
        script: 'stylelint "src/**/*.{pcss,css}"',
        description: 'Runs stylelint for the entire project.',
        fix: {
          script: 'nps "lint.css --fix"',
          description: 'Runs tslint in fix mode.',
        },
      },
      fix: {
        script: 'nps lint.ts.fix lint.css.fix',
        description: 'Runs tslint and stylelint in fix mode.',
      },
    },
    check: {
      script: 'nps lint test build',
      description: 'Runs the checks to validate the library (lint, test, build).',
    },
  },
}
