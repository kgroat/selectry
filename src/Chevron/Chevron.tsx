
import * as React from 'react'

interface ChevronProps {
  className?: string
}

export const Chevron = ({ className }: ChevronProps) => (
  <svg className={className} version='1.1' viewBox='0 0 1000 1000'>
    <polygon fill='currentcolor' points='800,550 500,300 200,550 200,700 500,450 800,700'/>
  </svg>
)
