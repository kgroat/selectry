
import * as React from 'react'
import * as cn from 'classnames'

import css from '../css'
import { injectContext } from '../SelectryContext/injectContext'
import { ContextType } from '../SelectryContext/SelectryContext'

export interface OptionProps<T> {
  children: NonNullable<React.ReactNode>
  value: T
  classNames?: {
    option?: string,
  }
  ctx?: ContextType
}

const noop = () => null

export function OptionUnwrapped<T> (props: OptionProps<T>) {
  const { children, value, classNames = {}, ctx = {} } = props
  const { onSelect = noop, selectedValue, open, testUl } = ctx

  const optionClassNames = cn(
    css.option,
    classNames.option,
    {
      [css.selected]: selectedValue === value,
    },
  )

  function handleClick (ev: React.MouseEvent) {
    ev.stopPropagation()
    onSelect(value)
  }

  return (
    <li className={optionClassNames} tabIndex={open && !testUl ? 0 : -1} onClick={handleClick} role='option'>
      {children}
    </li>
  )
}

OptionUnwrapped.displayName = 'Option'

export const Option = injectContext(OptionUnwrapped)
