
import * as React from 'react'

import { OptionProps } from './Option'

export function renderOption<T> (props: OptionProps<T>, key: string | number, value = key) {
  return (
    <option key={key} value={value}>{props.children}</option>
  )
}
