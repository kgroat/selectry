
import * as React from 'react'
import * as cn from 'classnames'

import css from '../css'
import { OptionElement } from '../Select/Select'
import { injectContext } from '../SelectryContext/injectContext'
import { ContextType } from '../SelectryContext/SelectryContext'

export interface OptionGroupProps<T> {
  children: OptionElement<T> | (OptionElement<T>[])
  label: string
  classNames?: {
    optionGroup?: string
    label?: string
    children?: string
  }
  ctx?: ContextType
}

export function OptionGroupUnwrapped<T> (props: OptionGroupProps<T>) {
  const { children, label, classNames = {} } = props

  const optionClassNames = cn(
    css.optionGroup,
    classNames.optionGroup,
  )

  const labelClassNames = cn(
    css.optionGroupLabel,
    classNames.label,
  )

  const childrenClassNames = cn(
    css.optionGroupChildren,
    classNames.children,
  )

  return (
    <li className={optionClassNames}>
      <div className={labelClassNames}>
        {label}
        <hr className={css.optionGroupLabelDivider} />
      </div>
      <ul className={childrenClassNames}>{children}</ul>
    </li>
  )
}

OptionGroupUnwrapped.displayName = 'OptionGroup'

export const OptionGroup = injectContext(OptionGroupUnwrapped)
