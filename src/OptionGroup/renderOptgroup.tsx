
import * as React from 'react'

import { OptionGroupProps } from './OptionGroup'
import { renderSelectChildren } from '../Select/renderSelectChild'
import { SelectChild } from '../Select/Select'

export function renderOptgroup<T> (props: OptionGroupProps<T>, key: string | number, valuePrefix: string) {
  const childrenArray = React.Children.toArray(props.children) as SelectChild<T>[]

  return (
    <optgroup label={props.label} key={key}>
      {renderSelectChildren(childrenArray, valuePrefix)}
    </optgroup>
  )
}
