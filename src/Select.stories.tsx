
import * as React from 'react'
import { withKnobs } from '@storybook/addon-knobs'
import { withInfo } from '@storybook/addon-info'
import { action } from '@storybook/addon-actions'
import { withState, StateConsumer } from '@storybook/addon-state'

import { Select } from './Select'
import { Option } from './Option'
import { OptionGroup } from './OptionGroup'

export default {
  title: 'Select',
  component: Select,
  decorators: [withKnobs],
}

interface StoryState {
  value: string
}

export function stateful (ctx: any) {
  console.log('ctx', ctx)
  const changed = action('onChange')
  return (
    <StateConsumer<StoryState>>
      {({ state, setState }) => (
        <Select value={state.value} onChange={value => { changed(value); setState({ value }) }}>
          <Option value='0'>0</Option>
          <OptionGroup label='one'>
            <Option value='10'>10</Option>
            <Option value='11'>11</Option>
          </OptionGroup>
          <OptionGroup label='two'>
            <Option value='20'>20</Option>
            <Option value='21'>21</Option>
          </OptionGroup>
        </Select>
      )}
    </StateConsumer>
  )
}
stateful.story = {
  // decorators: [withStore],
  decorators: [withInfo, withState<StoryState>({ value: '10' })],
}
