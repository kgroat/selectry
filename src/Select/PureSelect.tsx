
import * as React from 'react'
import * as ReactDOM from 'react-dom'
import * as cn from 'classnames'

import css from '../css'

import { SelectProps, SelectState, SelectChild, BaseProps, IntrinsicSelectProps } from './Select'
import { renderSelectChildren } from './renderSelectChild'
import { getLabel } from './getLabel'
import { Context, ContextType } from '../SelectryContext'
import { Chevron } from '../Chevron'

export type PureSelectProps<T> = SelectProps<T> & SelectState<T> & {
  onToggle: (open: boolean) => any,
}

const noop = () => null

function isChild (parent: HTMLElement, child: HTMLElement | null | undefined): boolean {
  if (child === null || child === undefined) {
    return false
  }
  const { parentElement } = child
  if (parentElement === parent) {
    return true
  } else if (child === document.body || parentElement === document.body) {
    return false
  }
  return isChild(parent, parentElement)
}

function getSelectElementProps<T> (props: PureSelectProps<T>) {
  const selectProps = { ...props } as IntrinsicSelectProps & PureSelectProps<T>

  delete selectProps.id
  delete selectProps.className
  delete selectProps.style
  delete selectProps.size
  delete selectProps.children
  delete selectProps.autoFocus
  delete selectProps.emptyLabel
  delete selectProps.value
  delete selectProps.flowListbox
  delete selectProps.classNames
  delete selectProps['aria-hidden']
  delete selectProps.tabIndex
  delete selectProps.role

  return selectProps as BaseProps
}

let selectIdCounter = 0

interface PureSelectState {
  listboxHeight: number
}

export class PureSelect<T> extends React.Component<PureSelectProps<T>, PureSelectState> {
  state: PureSelectState = { listboxHeight: -1 }
  private readonly _ref = React.createRef<HTMLDivElement>()
  private readonly _defaultId = `select-${selectIdCounter++}`
  private readonly _testUl = React.createRef<HTMLUListElement>()

  componentDidMount () {
    this.setState({ listboxHeight: this._getListboxHeight() })
  }

  componentDidUpdate (prevProps: PureSelectProps<T>) {
    if (this.props.children !== prevProps.children) {
      this.setState({ listboxHeight: this._getListboxHeight() })
    }
  }

  render () {
    const {
      id = this._defaultId,
      children,
      emptyLabel = 'Select...',
      value,
      flowListbox = false,
      classNames = {},
      open,
      ['aria-labelledby']: ariaLabelledby,
      style,
    } = this.props

    const { listboxHeight } = this.state

    const childrenArray = React.Children.toArray(children) as SelectChild<T>[]
    const htmlOptions = renderSelectChildren(childrenArray)

    const label = getLabel(childrenArray, value)

    const wrapperClassNames = cn(
      css.wrapper,
      classNames.wrapper,
    )

    const selectClassNames = cn(
      css.select,
      classNames.select,
      {
        [css.open]: open,
      },
    )

    const labelClassNames = cn(
      css.label,
      classNames.label,
      {
        [css.empty]: !label,
        [classNames.emptyLabel!]: !!classNames.emptyLabel && !label,
      },
    )

    const chevronClassNames = cn(
      css.chevron,
      classNames.chevron,
      {
        [css.flipped]: !open,
      },
    )

    const listboxClassNames = cn(
      css.listbox,
      classNames.listbox,
      {
        [css.flow]: flowListbox,
      },
    )

    const testUlClassnames = cn(
      listboxClassNames,
      css.listboxTest,
    )

    return (
      <Context.Provider value={this._buildContext(false)}>
        <div id={`${id}-wrapper`} ref={this._ref} className={wrapperClassNames} onBlur={this._handleBlur} style={style}>
          <select {...getSelectElementProps(this.props)} id={`${id}-select`} className={css.realSelect} aria-hidden={true}>
            {htmlOptions}
          </select>
          <button id={id} className={selectClassNames} type='button' tabIndex={0} onClick={this._handleClick} aria-haspopup='listbox' aria-labelledby={ariaLabelledby}>
            <div className={labelClassNames}>{label || emptyLabel}</div>
            <Chevron className={chevronClassNames} />
          </button>
          <ul id={`${id}-list`} className={listboxClassNames} role='listbox' style={{ height: open ? listboxHeight : 0 }}>
            {children}
          </ul>
          <Context.Provider value={this._buildContext(true)}>
            <ul className={testUlClassnames} ref={this._testUl} aria-hidden={true}>
              {children}
            </ul>
          </Context.Provider>
        </div>
      </Context.Provider>
    )
  }

  private _getListboxHeight = () => (
    this._testUl.current!.clientHeight + 2
  )

  private _handleClick = (ev: React.MouseEvent) => {
    const { onToggle = noop, open } = this.props
    onToggle(!open)
  }

  private _buildContext = (testUl: boolean): ContextType => ({
    open: this.props.open,
    testUl,
    onSelect: this._handleChange,
    selectedValue: this.props.value,
  })

  private _handleChange = (val: T) => {
    const { onToggle = noop, onChange = noop } = this.props

    onToggle(false)
    onChange(val)
  }

  private _handleBlur = (ev: React.FocusEvent<HTMLDivElement>) => {
    const { onToggle = noop } = this.props

    if (isChild(this._ref.current || ev.currentTarget, ev.relatedTarget as HTMLElement)) {
      return
    }

    onToggle(false)
  }

}
