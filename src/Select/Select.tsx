
import * as React from 'react'
import { OptionProps } from '../Option'
import { OptionGroupProps } from '../OptionGroup'
import { PureSelect } from './PureSelect'

export type OptionElement<T> = React.ReactElement<OptionProps<T>>
export type OptionGroupElement<T> = React.ReactElement<OptionGroupProps<T>>
export type SelectChild<T> = OptionElement<T> | OptionGroupElement<T>

export type IntrinsicSelectProps = JSX.IntrinsicElements['select']
type ExcludedProps =
  | 'className'
  | 'style'
  | 'value'
  | 'onChange'
  | 'ref'
export type BaseProps = Omit<IntrinsicSelectProps, ExcludedProps>

export interface SelectProps<T> extends BaseProps {
  children: SelectChild<T> | (SelectChild<T>[])
  /**
   * The text shown whenever no option is selected
   * @default 'Select...'
   */
  emptyLabel?: React.ReactChild
  onChange?: (newVal: T) => void
  value?: T | null
  flowListbox?: boolean
  classNames?: {
    wrapper?: string
    select?: string
    listbox?: string
    label?: string
    emptyLabel?: string
    chevron?: string
  }
  style?: React.CSSProperties
}

export interface SelectState<T> {
  value: T | null | undefined
  open: boolean
  focusOn: number[]
}

const noop = () => null
export class Select<T> extends React.Component<SelectProps<T>, SelectState<T>> {
  state: SelectState<T> = {
    value: this.props.value,
    open: false,
    focusOn: [],
  }

  componentDidUpdate (prev: SelectProps<T>) {
    if (prev.value !== this.props.value && this.props.value !== this.state.value) {
      this.setState(prev => ({
        ...prev,
        value: this.props.value,
      }))
    }
  }

  render = () => <PureSelect {...this.props} {...this.state} onChange={this._handleChange} onToggle={this._handleToggle} />
  private _handleToggle = (open: boolean) => this.setState({ open })
  private _handleChange = (value: T) => {
    const { onChange = noop } = this.props
    this.setState(prev => ({ ...prev, value }))
    onChange(value)
  }
}
