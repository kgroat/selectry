
import * as React from 'react'

import { Option } from '../Option'
import { OptionGroup } from '../OptionGroup'

import { SelectChild, OptionElement, OptionGroupElement } from './Select'

function getOptionLabel<T> (child: OptionElement<T>, value: T): React.ReactNode | null {
  if (child.props.value === value) {
    return child.props.children
  } else {
    return null
  }
}

function getLabelSingle<T> (child: SelectChild<T>, value: T): React.ReactNode | null {
  if (child.type === Option) {
    child = child as OptionElement<T>
    return getOptionLabel(child, value)
  } else if (child.type === OptionGroup) {
    child = child as OptionGroupElement<T>
    return getLabel(child.props.children, value)
  } else {
    return null
  }
}

export function getLabel<T> (children: SelectChild<T> | (SelectChild<T>[]), value: T) {
  if (!Array.isArray(children)) {
    return getLabelSingle(children, value)
  }

  for (const child of children) {
    const label = getLabelSingle(child, value)
    if (label !== null) {
      return label
    }
  }

  return null
}
