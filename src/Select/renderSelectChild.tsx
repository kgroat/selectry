
import { Option, OptionProps } from '../Option'
import { OptionGroup, OptionGroupProps } from '../OptionGroup'
import { renderOption } from '../Option/renderOption'
import { renderOptgroup } from '../OptionGroup/renderOptgroup'

import { SelectChild } from './Select'

function renderSelectChild<T> (child: SelectChild<T>, index: number, valuePrefix: string) {
  const value = valuePrefix ? `${valuePrefix}|${index}` : `${index}`
  if (child.type === Option) {
    return renderOption(child.props as OptionProps<T>, child.key || index, value)
  } else if (child.type === OptionGroup) {
    return renderOptgroup(child.props as OptionGroupProps<T>, child.key || index, value)
  } else {
    return null
  }
}

export function renderSelectChildren<T> (children: SelectChild<T> | SelectChild<T>[], valuePrefix = '') {
  if (!Array.isArray(children)) {
    return renderSelectChild(children, 0, valuePrefix)
  }

  return children.map((c, i) => renderSelectChild(c, i, valuePrefix)).filter(t => t !== null)
}
