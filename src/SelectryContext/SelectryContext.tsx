
import * as React from 'react'

export interface ContextType {
  open?: boolean
  testUl?: boolean
  onSelect?: (val: any) => void
  registerOption?: (location: any[]) => void
  focusOn?: number[]
  selectedValue?: any
}

const noop = () => null

export const Context = React.createContext<ContextType>({
  open: false,
  testUl: false,
  onSelect: noop,
  registerOption: noop,
  focusOn: [],
  selectedValue: null,
})
