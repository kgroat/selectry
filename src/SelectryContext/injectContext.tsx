
import * as React from 'react'
import { Context } from './SelectryContext'

export interface ContextProp<ContextType> {
  /**
   * @private
   * Injected using alta's `injectContext` HOC
   */
  ctx?: ContextType
}

/**
 * A Higher Order Component used to inject context into a wrapped component.
 * @param Context The Context object created by React.createContext()
 */
function injectAnyContext<ContextType> (Context: React.Context<ContextType>) {
  return <P extends ContextProp<ContextType>, C extends React.ComponentType<P>>(base: C): C => {
    const BaseComponent = base as React.ComponentClass<P>

    const wrapper = function ContextWrapper (props: P) {
      return (
        <Context.Consumer>
          {
            ctx => (
              <BaseComponent {...props} ctx={ctx} />
            )
          }
        </Context.Consumer>
      )
    } as C

    wrapper.displayName = BaseComponent.displayName || BaseComponent.name

    return wrapper
  }
}

export const injectContext = injectAnyContext(Context)
