
import * as React from 'react'

export function Stateful (props) {
  const { children, initialState } = props
  const [state, setState] = React.useState(initialState)

  return <>
      {children(state, setState)}
  </>
}
