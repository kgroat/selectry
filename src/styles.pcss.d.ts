declare const styles: {
  readonly "realSelect": string;
  readonly "wrapper": string;
  readonly "select": string;
  readonly "open": string;
  readonly "label": string;
  readonly "empty": string;
  readonly "listbox": string;
  readonly "listboxTest": string;
  readonly "flow": string;
  readonly "option": string;
  readonly "selected": string;
  readonly "optionGroup": string;
  readonly "optionGroupLabel": string;
  readonly "optionGroupLabelDivider": string;
  readonly "optionGroupChildren": string;
  readonly "chevron": string;
  readonly "flipped": string;
};
export = styles;

